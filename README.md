workout-schedule-creator

**Introduction**

The workout schedule creator is a series of scripts that automatically take
workout programs (from a huge variety) and converts them to a calendar format
effectively scheduling it with a few clicks instead of creating your own
calendar events and setting up the progressions manually.

**Programs**

**/r/BodyWeighFitness Routines:**
*Recommended Routine
The recommended routine from Reddit's bodyweight fitness communities is one of
the best programs available for beginners who want a straightforward strength 
and muscle building program and have little to no access to a gym or equipment.

*Couch To 5k
This is a program geared for someone with little to know running experience (or is out of shape)
to slowly build up to being able to run a 5k within 8-10 weeks. It works well 
for people who haven't worked out in a long time and have gotten out of shape to get back 
into the habit of working out regularly. The program is challenging, but the 
increase in difficulty through the program is gradual enough to keep it from being
discouraging for anyone. An excellent program for anyone looking to start running.

